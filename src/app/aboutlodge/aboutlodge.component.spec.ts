import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutlodgeComponent } from './aboutlodge.component';

describe('AboutlodgeComponent', () => {
  let component: AboutlodgeComponent;
  let fixture: ComponentFixture<AboutlodgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutlodgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutlodgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
