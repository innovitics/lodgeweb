import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule,Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { FormsModule } from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSliderModule} from '@angular/material/slider';
import {MatStepperModule} from '@angular/material/stepper';


import { SlickCarouselModule } from 'ngx-slick-carousel';
import { Ng5SliderModule } from 'ng5-slider';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomepagrComponent } from './homepagr/homepagr.component';
import { MoreComponent } from './more/more.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { AboutlodgeComponent } from './aboutlodge/aboutlodge.component';
import { PopupsignoutComponent } from './popupsignout/popupsignout.component';
import { PopupdeactivatepostComponent } from './popupdeactivatepost/popupdeactivatepost.component';
import { PopupsigninComponent } from './popupsignin/popupsignin.component';
import { PopupsignupComponent } from './popupsignup/popupsignup.component';
import { PopupresetpasswordComponent } from './popupresetpassword/popupresetpassword.component';
import { PopupenterverificationComponent } from './popupenterverification/popupenterverification.component';
import { DeveloperComponent } from './developer/developer.component';
import { DevelopercenterComponent } from './developercenter/developercenter.component';
import { QuicksearchComponent } from './quicksearch/quicksearch.component';
import { DeveloperdetailsComponent } from './developerdetails/developerdetails.component';
import { DeveloperdetailscenterComponent } from './developerdetailscenter/developerdetailscenter.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { FilterpartComponent } from './filterpart/filterpart.component';
import { OurpartnersComponent } from './ourpartners/ourpartners.component';
import { FavoritdetailsComponent } from './favoritdetails/favoritdetails.component';
import { FindlodgeComponent } from './findlodge/findlodge.component';
import { ExploreresultComponent } from './exploreresult/exploreresult.component';
import { ExploredetailsComponent } from './exploredetails/exploredetails.component';
import { MonthlypaymentComponent } from './monthlypayment/monthlypayment.component';
import { PopupletstalkComponent } from './popupletstalk/popupletstalk.component';
import { PopupsentsuccessComponent } from './popupsentsuccess/popupsentsuccess.component';
import { AddlodgeComponent } from './addlodge/addlodge.component';
import { PopupnolodgeComponent } from './popupnolodge/popupnolodge.component';


const routes: Routes = [
	{path:'footer',component:FooterComponent},
  {path:'navbar',component:NavbarComponent},
  {path:'homepage',component:HomepagrComponent},
  {path:'more',component:MoreComponent},
  {path:'privacy',component:PrivacyComponent},
  {path:'terms',component:TermsComponent},
  {path:'about lodge',component:AboutlodgeComponent},
  //{path:'deactivatepost',component:PopupdeactivatepostComponent},
  //{path:'signin',component:PopupsigninComponent},
  //{path:'signup',component:PopupsignupComponent},
  //{path:'resetpassword',component:PopupresetpasswordComponent},
  {path:'enterverification',component:PopupenterverificationComponent},
  {path:'developer',component:DeveloperComponent},
  {path:'developerdetails',component:DeveloperdetailsComponent},
  {path:'fav',component:FavoritesComponent},
  {path:'favdetails',component:FavoritdetailsComponent},
  {path:'find',component:FindlodgeComponent},
  {path:'explore',component:ExploreresultComponent},
  {path:'exploredetails',component:ExploredetailsComponent},
  {path:'payment',component:MonthlypaymentComponent},
  {path:'talk',component:PopupnolodgeComponent},
  {path:'add',component:AddlodgeComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavbarComponent,
    HomepagrComponent,
    MoreComponent,
    PrivacyComponent,
    TermsComponent,
    AboutlodgeComponent,
    PopupsignoutComponent,
    PopupdeactivatepostComponent,
    PopupsigninComponent,
    PopupsignupComponent,
    PopupresetpasswordComponent,
    PopupenterverificationComponent,
    DeveloperComponent,
    DevelopercenterComponent,
    QuicksearchComponent,
    DeveloperdetailsComponent,
    DeveloperdetailscenterComponent,
    FavoritesComponent,
    FilterpartComponent,
    OurpartnersComponent,
    FavoritdetailsComponent,
    FindlodgeComponent,
    ExploreresultComponent,
    ExploredetailsComponent,
    MonthlypaymentComponent,
    PopupletstalkComponent,
    PopupsentsuccessComponent,
    AddlodgeComponent,
    PopupnolodgeComponent,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatSelectModule,
    MatTabsModule,
		HttpClientModule,
		FormsModule,
    NgbModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    SlickCarouselModule,
    MatSliderModule,
    Ng5SliderModule,
    MatStepperModule,
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
    })

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http,'./assets/i18n/', '.json');
}
