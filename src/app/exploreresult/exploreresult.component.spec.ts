import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreresultComponent } from './exploreresult.component';

describe('ExploreresultComponent', () => {
  let component: ExploreresultComponent;
  let fixture: ComponentFixture<ExploreresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExploreresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
