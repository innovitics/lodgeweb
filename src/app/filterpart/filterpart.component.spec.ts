import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterpartComponent } from './filterpart.component';

describe('FilterpartComponent', () => {
  let component: FilterpartComponent;
  let fixture: ComponentFixture<FilterpartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterpartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterpartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
