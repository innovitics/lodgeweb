import { Component, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-findlodge',
  templateUrl: './findlodge.component.html',
  styleUrls: ['./findlodge.component.css']
})
export class FindlodgeComponent implements OnInit {

public emptylodge = false;
public findlodge = false;

value3: number = 200000;
  highValue3: number = 700000;
  options3: Options = {
    floor: 100000,
    ceil: 1000000
  };

  constructor() { }

  ngOnInit() {
  this.findlodge = true ;
  }
   toggle(type : any ) {
      
    switch(type){

      case 'emptylodge' :
        this.emptylodge = true ; 
        this.findlodge = false;
         
        break;

      case 'findlodge' :
        this.emptylodge = false ; 
        this.findlodge = true;
 
        break;

      default :
        break;

    } 
      
  }

}
