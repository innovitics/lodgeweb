import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepagrComponent } from './homepagr.component';

describe('HomepagrComponent', () => {
  let component: HomepagrComponent;
  let fixture: ComponentFixture<HomepagrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepagrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepagrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
