import { Component, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-monthlypayment',
  templateUrl: './monthlypayment.component.html',
  styleUrls: ['./monthlypayment.component.css']
})
export class MonthlypaymentComponent implements OnInit {
  value: number = 123;
  options: Options = {
    floor: 0,
    ceil: 20
  };
  constructor() { }

  ngOnInit() {
  }

}
