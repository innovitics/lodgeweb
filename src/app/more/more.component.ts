import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-more',
  templateUrl: './more.component.html',
  styleUrls: ['./more.component.css']
})
export class MoreComponent implements OnInit {
  public show:boolean = false;
  public buttonName:any = 'Show';

  public editProfile = false ;
  public Ads = false ;
  public Fav = false ;
  public Help = false;
  public AdsDetail = false;

 public show2:boolean = true ;
  public buttonName2:any = 'Show';

  constructor() { }

  ngOnInit() {
    this.editProfile = true ;
  }

  toggle(type : any ) {
      
    switch(type){

      case 'editProfile' :
        this.editProfile = true ; 
        this.Ads = false;
        this.Fav = false ;
        this.Help = false ;
        this.AdsDetail = false ;  
        break;

      case 'Ads' :
        this.editProfile = false ; 
        this.Ads = true;
        this.Fav = false ;
        this.Help = false ; 
        this.AdsDetail = false ;
        break;

      case 'Fav' :
        this.editProfile = false ; 
        this.Ads = false;
        this.Fav = true ;
        this.Help = false ; 
        this.AdsDetail = false ;
        break;

      case 'Help' :
        this.editProfile = false ; 
        this.Ads = false;
        this.Fav = false ;
        this.Help = true ; 
        this.AdsDetail = false ;
        break;

      default :
        break;

    } 
      
  }

}
