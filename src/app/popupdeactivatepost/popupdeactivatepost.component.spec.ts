import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupdeactivatepostComponent } from './popupdeactivatepost.component';

describe('PopupdeactivatepostComponent', () => {
  let component: PopupdeactivatepostComponent;
  let fixture: ComponentFixture<PopupdeactivatepostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupdeactivatepostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupdeactivatepostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
