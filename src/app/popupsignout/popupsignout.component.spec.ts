import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupsignoutComponent } from './popupsignout.component';

describe('PopupsignoutComponent', () => {
  let component: PopupsignoutComponent;
  let fixture: ComponentFixture<PopupsignoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupsignoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupsignoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
