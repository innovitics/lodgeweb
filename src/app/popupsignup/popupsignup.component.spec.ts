import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupsignupComponent } from './popupsignup.component';

describe('PopupsignupComponent', () => {
  let component: PopupsignupComponent;
  let fixture: ComponentFixture<PopupsignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupsignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupsignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
